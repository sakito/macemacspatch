#  Mac環境での Emacs build Makefile

## Commands Variables
TAR = tar xvf
CURL = curl -L -O

## Emacs
EMACS_VERSION=26.3
MAC_PORT_VERSION=7.9

## Emacs Lisp
SKK_SRC=ddskk-17.1_Neppu
SKK_CFG=SKK-CFG
HOWM_SRC=howm-1.4.4
AUCTeX_SRC=auctex-11.88
ORG_SRC=org-8.2.10

# Emacs SRC
EMACS_SRC = emacs-${EMACS_VERSION}
MAC_PORT=${EMACS_SRC}-mac-${MAC_PORT_VERSION}

## Emacs 共通
USER-EMACS-DIRECTORY=${HOME}/.emacs.d
LISPDIR=${USER-EMACS-DIRECTORY}/lisp
VERSION_SPECIFIC_LISPDIR=${USER-EMACS-DIRECTORY}/lisp
BINDIR=${USER-EMACS-DIRECTORY}/bin
SHAREDIR=${USER-EMACS-DIRECTORY}/share
INFODIR=${USER-EMACS-DIRECTORY}/share/info

TMP_DIR=tmp

.PHONY: all
all: getsrc patch compile

.PHONY: getsrc
getsrc:
	if [ ! -d ${TMP_DIR} ]; then mkdir ${TMP_DIR}; fi;
#	if [ ! -f ${EMACS_SRC}.tar.xz ]; then ${CURL} ftp://alpha.gnu.org/gnu/emacs/pretest/${EMACS_SRC}.tar.xz ; fi;
	cd ${TMP_DIR}; \
	if [ ! -f ${EMACS_SRC}.tar.xz ]; then ${CURL} http://ftp.gnu.org/pub/gnu/emacs/${EMACS_SRC}.tar.xz ; fi; \
	if [ ! -d ${EMACS_SRC} ]; then ${TAR} ${EMACS_SRC}.tar.xz ; fi; \
	if [ ! -f ${MAC_PORT}.tar.gz ]; then ${CURL} ftp://ftp.math.s.chiba-u.ac.jp/emacs/${MAC_PORT}.tar.gz ; fi; \
	if [ ! -d ${MAC_PORT} ]; then ${TAR} ${MAC_PORT}.tar.gz ; fi;

.PHONY: patch
patch:
	cd ${TMP_DIR}/${EMACS_SRC} ; \
	patch -p 1 < ../${MAC_PORT}/patch-mac ; \
	cp -r ../${MAC_PORT}/mac mac ; \
	cp ../${MAC_PORT}/src/* src ; \
	cp ../${MAC_PORT}/lisp/term/mac-win.el lisp/term ; \
	cp nextstep/Cocoa/Emacs.base/Contents/Resources/Emacs.icns mac/Emacs.app/Contents/Resources/Emacs.icns ; \


.PHONY: compile
compile:
	cd ${TMP_DIR}/${EMACS_SRC} ; \
	CC="clang" ; \
	./configure --with-mac --without-x --with-wide-int 2>&1 | tee ../config_log.txt; \
	make bootstrap 2>&1 | tee ../make_log.txt;

.PHONY: install
install:
	cd ${TMP_DIR}/${EMACS_SRC} ; \
	sudo make install ;
#	sudo cp -R mac/Emacs.app /Applications

# lisp
.PHONY: installlib
installlib: skkinstall auctexinstall howminstall w3minstall


# skk
.PHONY: skkinstall
skkinstall:
	if [ ! -d ${TMP_DIR} ]; then mkdir ${TMP_DIR}; fi;
	cd ${TMP_DIR}; \
	git clone https://github.com/skk-dev/ddskk.git ; \
	${CURL} https://skk-dev.github.io/dict/SKK-JISYO.L.gz ; \
	gunzip SKK-JISYO.L.gz ; \
	cd ddskk ; \
	mv ../SKK-JISYO.L dic ; \
	echo '(setq SKK_DATADIR "$(SHAREDIR)/skk")' >> $(SKK_CFG) ; \
	echo '(setq SKK_INFODIR "$(INFODIR)")' >> $(SKK_CFG) ; \
	echo '(setq SKK_LISPDIR "$(LISPDIR)/skk")' >> $(SKK_CFG) ; \
	echo '(setq SKK_SET_JISYO t)' >> $(SKK_CFG) ; \
	make LISPDIR=${LISPDIR} VERSION_SPECIFIC_LISPDIR=${VERSION_SPECIFIC_LISPDIR} INFODIR=${INFODIR} ; \
	make install LISPDIR=${LISPDIR} VERSION_SPECIFIC_LISPDIR=${VERSION_SPECIFIC_LISPDIR} INFODIR=${INFODIR} ;

# howm
.PHONY: howminstall
howminstall:
	${CURL} http://howm.sourceforge.jp/a/${HOWM_SRC}.tar.gz ; \
	${TAR} ${HOWM_SRC}.tar.gz ; \
	cd ${HOWM_SRC} ; \
	./configure --with-lispdir=${LISPDIR} --with-docdir=${SHAREDIR}/howm --with-extdir=${BINDIR} ; \
	make ; \
	make install ;

# AUCTeX
.PHONY: auctexinstall
auctexinstall:
	if [ ! -f ${AUCTeX_SRC}.tar.gz ]; then ${CURL} http://ftp.gnu.org/pub/gnu/auctex/${AUCTeX_SRC}.tar.gz ; fi; \
	${TAR} ${AUCTeX_SRC}.tar.gz ; \
	cd ${AUCTeX_SRC} ; \
	./configure --prefix=${USER-EMACS-DIRECTORY} --with-lispdir=${LISPDIR} --with-texmf-dir=/usr/local/texlive/2014basic/texmf-local ; \
	make ; \
	sudo make install ;

# Org
.PHONY: orginstall
orginstall:
	${CURL} http://orgmode.org/${ORG_SRC}.tar.gz ; \
	${TAR} ${ORG_SRC}.tar.gz ; \
	cd ${ORG_SRC} ; \
	make ;

# emacs-w3m
w3minstall:
	cvs -d :pserver:anonymous@cvs.namazu.org:/storage/cvsroot co emacs-w3m ; \
	cd emacs-w3m ; \
	autoconf ; \
	./configure --with-lispdir=${LISPDIR}/w3m --datarootdir=${SHAREDIR} --with-icondir=${SHAREDIR}/icon ; \
	make ; \
	make install ; \
	make install-icons ;
